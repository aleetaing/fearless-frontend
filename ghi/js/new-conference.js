window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        const selectElement = document.getElementById('location');

        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectElement.appendChild(option);
        }

    } else {
        console.log('error fetching locations list ')
    }

    // This event happens when user hits Enter in an input field of the form
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
        // prevents the default behavior of the browser, which would send the form data to the server
        // We want to send the data to the RESTful API
        event.preventDefault();
        // Convert the form data to JSON for our call to the API
        // Select the form element
        const formData = new FormData(formTag);
        // Create a new FormData object from the Form element
        const formObj = Object.fromEntries(formData);
        // Convert FormData object to JSON
        const json = JSON.stringify(formObj);
        // Send the data to the server
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset(); // resets the form to its original state (clearing the form)
            const newConference = await response.json();
            console.log(newConference);
        }
    });
})
