function createCard(name, description, pictureUrl, startTime, endTime, location) {

    return `
        <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    <p class="card-text">${startTime} - ${endTime}</p>
                </div>
                <div
            </div>
        </div>
        <div id="alertPlaceholder"></div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // This creates an alert div class with the 'danger' type
            const errorMessage = `
                <div class="alert alert-danger" role="alert">
                    Error getting the data from the API
                </div>`;
            // This creates an empty div tag
            const errorContainer = document.createElement("div");
            // This puts the empty div tag in the inner HTML and adds the error message inside
            errorContainer.innerHTML = errorMessage;
            // This creates a firstChild variable that represents the first element inside the body tag
            const firstChild = document.body.firstChild;
            // This inserts the error Container before the first child so that it appears at the top of the page
            document.body.insertBefore(errorContainer, firstChild);

        } else {
            const data = await response.json();

            let counter = 0 // keeps track of which column we want our content in

            for (let i = 0; i < data.conferences.length; i++) {
                const detailUrl = `http://localhost:8000${data.conferences[i].href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    const starts = details.conference.starts;
                    const ends = details.conference.ends;

                    const options = {
                        month: 'numeric',
                        day: '2-digit',
                        year: '2-digit',
                    };

                    const startTime = new Date(starts).toLocaleDateString(options);
                    const endTime = new Date(ends).toLocaleDateString(options);

                    const location = details.conference.location.name;

                    const html = createCard(
                        title,
                        description,
                        pictureUrl,
                        startTime,
                        endTime,
                        location
                    );
                    const columns = document.querySelectorAll('.col'); // columns is a NodeList of all locations where 'col' appears
                    const column = columns[counter] // column is an object; grabs the location from the querySelector (div col 0, div col 1, div col 2)
                    column.innerHTML += html; // put the card information in html into that specific column
                    counter++
                    if (counter === 3) {
                        counter = 0
                    }
                }
            }
        }
    } catch (e) {
        console.error(e);
    }
});
